$.EnemyBase = function( args ) {
    this.color = "#E06F8B";
    this.height = 100;
    this.width = 900;
    this.hw = this.width / 2;
    this.hh = this.height / 2;
    this.x = 0;
    this.y = 0;

    this.name = '';
    this.spawnTimer = 2;
    PLAYGROUND.Utils.merge( this, args );
    this.text = 'ENEMY: ' + this.name;
    this.disposition = this.startingDisposition
};

$.EnemyBase.prototype.step = function( dt ) {
    // check win/loss conditions:
    if ( this.disposition <= 0 ) {
        $.GameState.player.life = 0;
        $.GameState.lose();
    }
    else if ( this.disposition >= 100 ) {
        $.GameState.win();

    }

    // update timer:
    if ( this.spawnTimer > 0 ) {
        this.spawnTimer -= dt;
    }
    else {
        this.spawnUnit();
    }
};

$.EnemyBase.prototype.render = function() {
    var txt = this.text + ' \n ' + 'DISPOSITION: ' + this.disposition;
    $._
        .fillStyle( this.color )
        .fillRect( this.x, this.y, this.width, this.height )
        .fillStyle( '#222' )
        .font( '20px Arial')
        .textAlign( 'center' )
        .textBaseline( 'middle' )
        .wrappedText( txt, this.x + this.hw, this.y + this.height / 4, this.hw );

    // disposition bar
    var h = 10,
        w = this.disposition;
    $._
        .fillStyle('#222')
        .fillRect( this.x + this.hw - w, this.y + this.height * 0.75, w * 2, h );
};

///////////////////////////////////////////////////////////////////////////////
// Spawn units
///////////////////////////////////////////////////////////////////////////////

$.EnemyBase.prototype.spawnUnit = function( args ) {
    var x = $.Util.randInt( this.x + 20, this.width - 20 ),
        y = this.y + this.height - 20;
    $.GameState.entities.add( $.Unit, {
        color: this.color,
        x: x,
        y: y
    });
    this.spawnTimer = 2 + ( Math.random() * 2 );
};

///////////////////////////////////////////////////////////////////////////////
// Hurt
///////////////////////////////////////////////////////////////////////////////
$.EnemyBase.prototype.hurt = function( word ) {
    var text = word.text,
        list = word.list,
        val,
        red = '#BE2633',
        green = '#44891A';

    if ( this.angryWords.indexOf(text) !== -1 ) {
        val = -20;
        this.spawnUnit({
            speed: 70
        });
    }
    else if ( this.happyWords.indexOf(text) !== -1 ) {
        val = 20;
        $.GameState.removeAllUnits();
    }
    else {
        val = $.Util.randInt( this.lists[list].min, this.lists[list].max );
        if ( val > 0 ) {
            $.GameState.removeUnit();
        }
    }

    this.disposition += val;
    $.GameState.flash( 0.2, val > 0 ? green : red );
    app.sound.play( val > 0 ? 'positive' : 'negative');
};