$.PlayerBase = function( args ) {
    this.color = "#31A2F2";
    this.height = 100;
    this.width = 900;
    this.hw = this.width / 2;
    this.hh = this.height / 2;

    this.x = 0;
    this.y = 600 - this.height;

    this.life = 10;
    this.text = '';
    PLAYGROUND.Utils.merge( this, args );
};

$.PlayerBase.prototype.step = function( dt ) {

};

$.PlayerBase.prototype.render = function() {

    $._
        .fillStyle( this.color )
        .fillRect( this.x, this.y, this.width, this.height )
        .fillStyle( '#222' )
        .font( '20px Arial')
        .textAlign( 'center' )
        .textBaseline( 'middle' )
        .fillText( 'Choose your words wisely: ' + this.text + '_', 
            this.x + this.hw, 
            this.y + this.height * 0.25 
        );
    $._
        .textAlign('center')
        .fillText( 'DEFENSE: ' + this.life, this.x + this.hw, this.y + this.hh )

    // disposition bar
    var h = 10,
        w = this.life * 10;
    $._
        .fillStyle('#222')
        .fillRect( this.x + this.hw - w, this.y + this.height * 0.75, w * 2, h );
    
};

$.PlayerBase.prototype.hurt = function() {
    $.GameState.flash( 0.2, '#999' );
    app.sound.play('hurt');

    this.life -= 1;
    if ( this.life <= 0 ) {
        $.GameState.lose();
    }
};

///////////////////////////////////////////////////////////////////////////////
// letter functions
///////////////////////////////////////////////////////////////////////////////
$.PlayerBase.prototype.addLetter = function( letter ) {
    this.text += letter;
    this.checkWord();
};

$.PlayerBase.prototype.backspace = function( ) {
    this.text = this.text.substring( 0, this.text.length - 1 );
};

$.PlayerBase.prototype.clearText = function( ) {
    this.text = '';
};

///////////////////////////////////////////////////////////////////////////////
// Word functions
///////////////////////////////////////////////////////////////////////////////

$.PlayerBase.prototype.checkWord = function() {
    var words = $.GameState.wordList.children;

    for (var i = words.length - 1; i >= 0; i--) {
        if ( words[i].text.toLowerCase() === this.text.toLowerCase() ) {
            $.GameState.activateWord( this.text );
            this.clearText();
            break;
        }
    };
};