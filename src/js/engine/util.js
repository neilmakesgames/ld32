$.Util = {};

$.Util.angleTo = function( a, b ) {
    return Math.atan2(a.y - b.y, a.x - b.x);
};

$.Util.distanceTo = function( x1, y1, x2, y2 ) {
    if ( arguments.length > 2 ) {
        var dx = x1 - x2;
        var dy = y1 - y2;

        return Math.sqrt(dx * dx + dy * dy);
    } 
    else {
        var dx = x1.x - y1.x;
        var dy = x1.y - y1.y;

        return Math.sqrt(dx * dx + dy * dy);
    }
};

$.Util.randInt = function( min, max ) {
    return Math.floor( Math.random() * (max - min) ) + min;
};

$.Util.pointInRect = function( x, y, rx, ry, rw, rh ) {
    return !(x < rx || y < ry || x > rx + rw || y > ry + rh);
};

// Number extensions
Number.prototype.map = function ( in_min , in_max , out_min , out_max ) {
  return ( this - in_min ) * ( out_max - out_min ) / ( in_max - in_min ) + out_min;
};

Number.prototype.limit = function( min, max ) {
    return Math.min( max, Math.max( min, this ) );
};

// Array extension
Array.prototype.random = function() {
    return this[ Math.floor(Math.random() * this.length) ];
};

// localstorage extensions
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};