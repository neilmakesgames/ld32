// gulpfile.js

var gulp = require( 'gulp' ),
    webserver = require( 'gulp-webserver' ),
    concat = require( 'gulp-concat' ),
    changed = require( 'gulp-changed' );

// concatenate and copy JS

gulp.task( 'js', function() {
    gulp.src( ['src/js/**/*.js'], { 'base' : 'src/js' } )
    .pipe( changed('dist/js') )
    .pipe( gulp.dest('dist/js') );

});

gulp.task( 'data', function() {
    gulp.src( ['src/data/**/*.json'], { 'base' : 'src/data' } )
    .pipe( changed('dist/data') )
    .pipe( gulp.dest('dist/data') );

});

gulp.task( 'sounds', function() {
    gulp.src( ['src/sounds/**/*.ogg', 'src/sounds/**/*.mp3'], { 'base' : 'src/sounds' } )
    .pipe( changed('dist/sounds') )
    .pipe( gulp.dest('dist/sounds') );

});


// images - just copy changed files for now, can optimize later:
gulp.task( 'images', function() {
    gulp.src( ['src/images/**/*'], { 'base' : 'src/images' } )
    .pipe( changed('dist/images') )
    .pipe( gulp.dest('dist/images') );
});

// copy CSS folder and all sub-folders, e.g. webfonts
gulp.task( 'css', function() {
    gulp.src( ['src/css/**/*'], { 'base' : 'src/css' } )
    .pipe( changed('dist/css') )
    .pipe( gulp.dest('dist/css') );
});

// copy html, not recursively
gulp.task( 'html', function() {
    gulp.src( ['src/*.html'], { 'base' : 'src' } )
    .pipe( changed('dist') )
    .pipe( gulp.dest('dist') );
});

// web server - https://www.npmjs.com/package/gulp-webserver
gulp.task( 'serve', function() {
    gulp.src( 'dist' )
    .pipe( webserver({
        livereload: true,
        open: true
    }));
    // watch the directories and run the appropriate tasks
    // when something changes:
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/css/**/*', ['css']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/data/**/*.json', ['data']);
    gulp.watch('src/sounds/**/*', ['sounds']);
    gulp.watch('src/images/**/*', ['images']);
});


// Default task
gulp.task( 'default', ['html', 'images', 'css', 'js', 'data', 'sounds'] );