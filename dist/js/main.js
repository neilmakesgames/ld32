var app = new PLAYGROUND.Application({

    //smoothing: false,
    width: 900,
    height: 600,
    scale: 1.0,
    preventContextMenu: false,
    muted: false,
    tutorialFinished: false,

    create: function() {
        // preload assets here
        this.loadData('wordlist'); // saved as app.data.wordlist
        this.loadData('enemydata'); // saved as app.data.enemydata
        this.loadImages('neilmakesgames-transparent');
        this.loadSounds('a-seas-gorillas', 'positive', 'negative', 'hurt');

        $._ = this.layer;
        this.hw = this.width / 2;
        this.hh = this.height / 2;

        if ( window.localStorage ) {
            if ( localStorage.getObject('ld32') ) {
                this.loadSavedData();
            }
            else {
                localStorage.setObject('ld32', {
                    'muted': false,
                    'tutorialFinished' : false
                });
            }
        }

    },

    ready: function() {

        this.setState( $.MenuState );

    }, 
///////////////////////////////////////////////////////////////////////////////
// Audio
///////////////////////////////////////////////////////////////////////////////
    muteAudio:  function() {
        this.muted = !this.muted;

        if( this.muted ) {
            this.sound.setMaster( 0 );
            this.music.setMaster( 0 );
        } 
        else {
            this.sound.setMaster( 1 );
            this.music.setMaster( 1 );
        }
    },
///////////////////////////////////////////////////////////////////////////////
// save
///////////////////////////////////////////////////////////////////////////////
    saveData: function() {
        localStorage.setObject('ld32', {
            'muted': this.muted,
            'tutorialFinished': this.tutorialFinished
        });
    },

    loadSavedData: function() {
        this.muted = localStorage.getObject('ld32').muted || false;
        this.tutorialFinished = localStorage.getObject('ld32').tutorialFinished || 0;
    }


}); 
