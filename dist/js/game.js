$.GameState = {};

///////////////////////////////////////////////////////////////////////////////
// Create
///////////////////////////////////////////////////////////////////////////////
$.GameState.create = function() {
    this.flashColor = '#999';

    this.reset();
};
$.GameState.addInitialWords = function() {

    for ( var x = 50, y = 400, i = 0; x < 900; x += 100 ) {

        this.addWord({
            x: x ,
            y: (i % 2 == 0) ? y : y + 40
        });
        i += 1;
    };
};

$.GameState.reset = function() {
    this.hurtTimer = 0;
    this.gameOver = false;
    this.victory = false;
    this.gameOverTimer = 0;

    this.entities = new $.Entities();
    this.wordList = new $.Entities();

    if ( app.muted ) {
        app.sound.setMaster( 0 );
        app.music.setMaster( 0 );
    }

    this.player = this.entities.add( $.PlayerBase );

    if ( app.tutorialFinished == false ) {
        this.tutorial = true;
    }
    else {
        this.tutorial = false;
    }

    var e = this.tutorial ? 0 : $.Util.randInt(1, 4);

    this.enemy = this.entities.add( $.EnemyBase, app.data.enemydata[ e ] );
    this.addInitialWords();
    this.music = app.music.play('a-seas-gorillas', true);

};


///////////////////////////////////////////////////////////////////////////////
// Step
///////////////////////////////////////////////////////////////////////////////

$.GameState.step = function( dt ) {
    if ( this.hurtTimer > 0 ) {
        this.hurtTimer -= dt;
    }

    if ( !this.gameOver && !this.tutorial ) {
        this.entities.step( dt );
        this.wordList.step( dt );
    }
    else {
        if ( this.gameOverTimer > 0 ) {
            this.gameOverTimer -= dt;
        }
    }
};

///////////////////////////////////////////////////////////////////////////////
// Render
///////////////////////////////////////////////////////////////////////////////

$.GameState.render = function() {
    $._.clear( '#222' ).save();

    if ( this.hurtTimer > 0 ) {
        $._
            .a( this.hurtTimer.map(0.25, 0, 1, 0))
            .fillStyle( this.flashColor )
            .fillRect(0, 0, app.width, app.height)
            .ra();

        $._.translate(-5 + Math.random() * 10 | 0, -5 + Math.random() * 10 | 0);
    }

    this.entities.render();
    this.wordList.render();

    if ( this.gameOver ) {
        app.tutorialFinished = true;
        app.saveData();
        $._
            .clear( 'rgba(0, 0, 0, .5)' )
            .fillStyle( '#efefef' )
            .textAlign( 'center' )
            .textBaseline( 'middle' )
            .font( '20px Arial' );

        if ( this.victory == true ) {
            // win state
            $._.wrappedText( 'VICTORY!', app.hw, app.hh, app.hw );
        }
        else {
            // lose state
            $._.wrappedText( 'GAME OVER', app.hw, app.hh, app.hw );
        }

        if ( this.gameOverTimer <= 0 ) {
            $._.wrappedText( 'Press [ENTER] to play again', app.hw, app.hh + 40, app.hw );
        }
    }
    else if ( this.tutorial ) {
        var tutorialText = 'HOW TO PLAY: \n \n You are a diplomat, attempting to ' +
            'negotiate peace. \n \n Your enemy will periodically attack you. ' + 
            'Type the words you think your attacker wants to hear ' + 
            'the most in order to improve their disposition. \n \n ' + 
            'Words your enemy likes will halt their attacks. Words they dislike may ' + 
            'provoke them further. \n \n Suffer too many attacks and your nation will fall. ' +
            'Improve your foe\'s disposition to 100 and your enemy will cease their aggression. \n \n ' + 
            'Press [ENTER] to begin.';
        $._
            .clear( 'rgba(0, 0, 0, .8)' )
            .fillStyle( '#efefef' )
            .textAlign( 'left' )
            .textBaseline( 'top' )
            .font( '20px Arial' )
            .wrappedText(tutorialText, 20, 70, app.width - 40);
    }

    $._.restore();
};

$.GameState.flash = function( duration, color ) {
    this.hurtTimer = duration;
    this.flashColor = color;
};
///////////////////////////////////////////////////////////////////////////////
// win and lose
///////////////////////////////////////////////////////////////////////////////
$.GameState.win = function() {
    this.gameOver = true;
    this.victory = true;
    this.gameOverTimer = 2;
    app.enemy += 1;
    app.saveData();
};

$.GameState.lose = function() {
    this.gameOver = true;
    this.victory = false;
    this.gameOverTimer = 2;

};

///////////////////////////////////////////////////////////////////////////////
// add/remove word
///////////////////////////////////////////////////////////////////////////////

$.GameState.addWord = function( args ) {

    var list = app.data.wordlist.sublists.random();
    var text = app.data.wordlist[list].random();

    var currentWords = [];
    var words = this.wordList.children;

    for (var i = words.length - 1; i >= 0; i--) {
        currentWords.push( words[i].text );
    };

    while ( currentWords.indexOf( text ) !== -1 ) {
        list = app.data.wordlist.sublists.random();
        text = app.data.wordlist[list].random();
    }

    var args = PLAYGROUND.Utils.merge({
        list: list,
        text: text
    }, args);

    this.wordList.add( $.Word, args );

};

$.GameState.removeWord = function( wordEntity ) {

    var x = wordEntity.ox,
        y = wordEntity.oy;

    this.wordList.remove( wordEntity );

    this.addWord({
        x: wordEntity.ox,
        y: wordEntity.oy
    });

};

$.GameState.activateWord = function( text ) {
    var words = this.wordList.children;
    for (var i = words.length - 1; i >= 0; i--) {
        if ( words[i].text.toLowerCase() == text ) {
            words[i].activate();
            break;
        }
    };
};

///////////////////////////////////////////////////////////////////////////////
// Keyboard/input
///////////////////////////////////////////////////////////////////////////////
$.GameState.keydown = function( e ) {
    
    if ( this.tutorial && e.key === 'enter') {
        this.tutorial = false;
    }
    else if ( this.gameOver && this.gameOverTimer <= 0 && e.key === 'enter' ) {
        app.music.stop( this.music );
        this.reset();
    }
    else if ( e.original.which >= 48 && e.original.which <= 90 ) {
        // test to see if alphabetic
        this.player.addLetter( e.key );
    }
    else if ( e.key === 'backspace' ) {
        this.player.backspace();
    }
    else if ( e.key === 'escape' ) {
        this.player.clearText();
    }
    else if ( e.key === 'tab' ) {
        app.muteAudio();
    }
};

///////////////////////////////////////////////////////////////////////////////
// Remove Units
///////////////////////////////////////////////////////////////////////////////
$.GameState.removeUnit = function() {
    var units = this.entities.getEntitiesByType( $.Unit );
    if ( units.length > 0 ) {
        this.entities.remove( units[0] );
    }
};

$.GameState.removeAllUnits = function() {
    var units = this.entities.getEntitiesByType( $.Unit );
    for (var i = units.length - 1; i >= 0; i--) {
        this.entities.remove( units[i] );
    };

};

