$.MenuState = {};

///////////////////////////////////////////////////////////////////////////////
// Create
///////////////////////////////////////////////////////////////////////////////
$.MenuState.create = function() {
    var i = app.images['neilmakesgames-transparent'];
    this.logo = {
        img: i,
        x: app.width - i.width - 20,
        y: app.height - i.height - 20,
        w: i.width,
        h: i.height
    };

    this.isOnLudumDareSite = window.parent.location.href.indexOf('ludumdare.com') !== -1;

    
    this.ldlink = {
        text: 'Visit this game\'s Ludum Dare page',
        font: '12px Arial',
        x: 20,
        y: app.height - 40,
    };
    this.ldlink.w = $._
        .font( this.ldlink.font )
        .textBoundaries( this.ldlink.text )
        .width;
    this.ldlink.h = $._
        .font( this.ldlink.font )
        .textBoundaries( this.ldlink.text )
        .height;
    
};

///////////////////////////////////////////////////////////////////////////////
// Step
///////////////////////////////////////////////////////////////////////////////
$.MenuState.step = function( dt ) {

};

///////////////////////////////////////////////////////////////////////////////
// Render
///////////////////////////////////////////////////////////////////////////////
$.MenuState.render = function() {
    $._.clear( '#222' );
    $._
        .drawImage( this.logo.img, this.logo.x, this.logo.y )
        .fillStyle( '#efefef' )
        .textAlign( 'center' )
        .textBaseline( 'middle' )
        .font( '60px Arial')
        .wrappedText('What They Want To Hear', app.hw, app.hh - 50, app.width )
        .font( '12px Arial')
        .wrappedText('Made in 48 hours by Neil Knauth for Ludum Dare 32', app.hw, app.hh, app.hw)
        .font( '20px Arial' )
        .wrappedText( '[ENTER] to start \n \n [TAB] to mute', app.hw, app.hh + 100, app.hw );

    // only show this if the game is NOT embedded on the ludum dare site
    if ( !this.isOnLudumDareSite ) {
        $._    
            .font( this.ldlink.font )
            .textAlign( 'left' )
            .textBaseline('top')
            .fillText( this.ldlink.text, this.ldlink.x, this.ldlink.y ); 
    }

    if ( app.tutorialFinished ) {
        $._
            .fillStyle( '#efefef' )
            .textAlign( 'center' )
            .textBaseline( 'middle' )
            .font('20px Arial')
            .fillText('[R] to Reset tutorial', app.hw, app.hh + 190)
    }

};

///////////////////////////////////////////////////////////////////////////////
// input
///////////////////////////////////////////////////////////////////////////////
$.MenuState.keydown = function( e ) {

    if ( e.key === 'enter' ) {
        app.layer.canvas.style.cursor = 'default';
        app.setState( $.GameState );
    }
    else if ( e.key === 'tab' ) {
        app.muteAudio();
    }
    else if ( e.key === 'r' ) {
        app.tutorialFinished = false;
        app.saveData();
    }
};
$.MenuState.mousemove = function( data ) {
    if ( $.Util.pointInRect( data.x, data.y, this.logo.x, this.logo.y, this.logo.w, this.logo.h ) ) {
        app.layer.canvas.style.cursor = 'pointer';
    }
    else if ( !this.isOnLudumDareSite && $.Util.pointInRect( data.x, data.y, this.ldlink.x, this.ldlink.y, this.ldlink.w, this.ldlink.h ) ) {
        app.layer.canvas.style.cursor = 'pointer';
    }
    else {
        app.layer.canvas.style.cursor = 'default';
    }
};
$.MenuState.mousedown = function( data ) {
    if ( $.Util.pointInRect( data.x, data.y, this.logo.x, this.logo.y, this.logo.w, this.logo.h ) ) {
        window.open('http://neilmakesgames.tumblr.com', '_blank');
    }
    else if ( !this.isOnLudumDareSite && $.Util.pointInRect( data.x, data.y, this.ldlink.x, this.ldlink.y, this.ldlink.w, this.ldlink.h ) ) {
        // to do:
        window.open('http://ludumdare.com/compo/ludum-dare-32/?action=preview&uid=32759', '_blank');
    }
};