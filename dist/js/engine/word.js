$.Word = function( args ) {
    this.color = '#efefef';
    this.vx = 0;
    this.vy = 0;
    this.active = false;
    this.zIndex = 5;
    PLAYGROUND.Utils.merge( this, args );
    this.init();
};

$.Word.prototype.init = function() {
    this.ox = this.x;
    this.oy = this.y;
};

$.Word.prototype.step = function( dt ) {
    if ( this.active ) {
        this.y += this.vy * dt;
        this.vy -= 20;

        if ( this.y <= $.GameState.enemy.y + $.GameState.enemy.hh ) {
            this.kill();
        }
    }
};

$.Word.prototype.render = function() {
    $._
        .fillStyle( this.color )
        .textAlign( 'center' )
        .textBaseline( 'middle' )
        .fillText( this.text, this.x, this.y );
};

///////////////////////////////////////////////////////////////////////////////
// activate and kill
///////////////////////////////////////////////////////////////////////////////

$.Word.prototype.activate = function() {
    this.active = true;
    this.vy = -50;
};

$.Word.prototype.kill = function() {
    $.GameState.enemy.hurt( this );
    $.GameState.removeWord( this );
};