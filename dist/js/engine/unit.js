$.Unit = function( args ) {
    this.color = '#efefef';
    this.radius = 10;
    this.speed = 50;
    this.vx = 0;
    this.vy = 0;
    this.zIndex = 1;
    PLAYGROUND.Utils.merge( this, args );
    this.init();
};

$.Unit.prototype.init = function() {
    this.vx = 0;
    this.vy = this.speed;
};

$.Unit.prototype.step = function( dt ) {
    this.x += this.vx * dt;
    this.y += this.vy * dt;
    var p = $.GameState.player;

    //if ( $.Util.distanceTo( p, this ) < this.radius + p.radius ) {
    if ( this.y >= p.y ) {
        this.collection.remove( this );
        p.hurt();
    }
};

$.Unit.prototype.render = function() {
    $._
        .fillStyle( this.color )
        .fillCircle( this.x, this.y, this.radius );
};